/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

'use strict';

import React, {
  Component
} from 'react';

import {
  AppRegistry,
  SafeAreaView,
  NavigatorIOS,
} from 'react-native';
import Router from './container/Navigation/AppNavigation';

export default class App extends Component {
  render() {
    return ( <
      Router / >
    );
  }
}
AppRegistry.registerComponent('Forward', () => App);