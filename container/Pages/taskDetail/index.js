import React, { PropTypes, Component } from 'react';
import { Text, 
  View, 
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
  Dimensions, 
  Image, 
  ImageBackground, 
  TextInput, 
  TouchableOpacity, 
  FlatList, 
  ListItem, 
  BackHandler, 
  I18nManager, 
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
  CheckBox
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';
import { Images, Colors,Metrics } from '../../Themes';
import TaskListItem from './TaskListItem';
import Styles from './styles';
import { CalendarService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Toast from 'react-native-root-toast';
import API from '../../API';

export default class TaskDetail extends Component {
  _placeholderText = "Please provide comment on your task";
  _finalChallengeDay = 30;

  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent(this.state.content.day);
  }

  _populateDefaultState = async () => {
    this.state = {
      loading: false,
      refreshing: false,
      content: {
        day: this.props.navigation.getParam('day', 1),
        title: '',
        description: '',
        task: [],
        comment: '',
        image:'',
      }
    };
  }

  _loadContent = async (day) => {
    CalendarService
      .getCalendarDetail(day)
      .then(async (response) => {
        let newState = Object.assign({}, this.state);
        let comment = '';
        let completed = false;

        if (response.completion) {
          completed = response.completion.task1 == "1";
          comment = response.completion.comment;
        }

        newState.content.title = response.calendar.title;
        newState.content.description = response.calendar.description;
        newState.content.comment = comment;
        if(response.calendar.image !== null){
          newState.content.image = `${API.baseURL}/static${response.calendar.image}`;
        }
        else{
          newState.content.image = '';
        }
        
        
        newState.task = [];

        newState.content.task.push({
          id: day,
          title: response.calendar.title,
          task1: response.calendar.task1,
          completed: completed,
        });
        
        this.setState(newState);
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching task detail failed: " + message);
        console.log(error.response);
      });
  }

  componentWillMount() {
    let that = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      that.navigateBack(false);

      return true;
    });
  }

  navigateBack = (submitted) => {
    if (submitted) {
      this.props.navigation.state.params.refresh();
    }

    this.props.navigation.goBack();
  }

  _keyExtractor = (item, index) => index

  _renderItem = ({ item }) => (
    <TaskListItem
      item={item}
      onPressItem={this._onPressItem}
    />
  )

  _onPressItem = (id) => {
    let newState = Object.assign({}, this.state);

    let filtered = newState.content.task.filter(function(item) {
       return item.id == id;
    });

    if (filtered.count != 0) {
      filtered[0].completed = !filtered[0].completed;
    }

    this.setState(newState);
  } 

  submitCompletion = async () => {

    if (this.state.content.task[0].completed ===false) {
      alert("You must complete the task to submit.");
    } else {
      let user = await ProfileStorage.getLocalProfile();

      const { task, comment } = this.state.content;
      const userId = user.id;
      let that = this;

      CalendarService
        .submitProgress({ 
          day: task[0].id,
          userId: userId,
          comment: comment,
          task1: task[0].completed ? 1 : 0
        })
        .then((response) => {
          console.log(response);

          if (!response.status) {
            alert('Failed to submit daily task');
          }

          if (task[0].id == this._finalChallengeDay && task[0].completed) {
            that.props.navigation.navigate('FinalTaskCompleted');
          } else {
            if (response.reward) {
              const rewardMessage = `You earn a reward by completing day ${task[0].id}, check rewards tab to claim`;

              Toast.show(rewardMessage, {
                  duration: Toast.durations.LONG,
                  position: Toast.positions.BOTTOM,
                  shadow: true,
                  animation: true,
                  hideOnPress: true,
                  delay: 0
              });
            }
            
            that.navigateBack(true);
          }
        }).catch((error) => {
          let data = error.response.data;
          let message = data.msg ? data.msg : '';

          alert("Failure to submit progress. " + message);
          console.log(error.response);
        });
    }
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    let day = this.props.navigation.getParam('day', 1);
    let navigationTitle = "Day " + day;

    return (
      <Container>
        <ImageBackground style={Styles.imgContainer} source={imageUri}>
          <Header style={Styles.header}>
            <Left style={Styles.left}>
              <TouchableOpacity style={Styles.backArrow} onPress={() => this.navigateBack(false)}>
                <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
              </TouchableOpacity>
            </Left>
            <Body style={Styles.body}>
              <Text style={Styles.titleNavigationBar}>{navigationTitle}</Text>
            </Body>
            <Right style={Styles.right}/>
          </Header>
          <Content>
          <KeyboardAwareScrollView
              style={Styles.actionContainer}
              enableOnAndroid={true} 
              behavior="padding">
              
              <Text style={Styles.sectionHeader}>{this.state.content.title}</Text>
              <HTML html={this.state.content.description} />

              <Text style={Styles.sectionTaskListHeader}>So here is your challenge</Text>
              
              <FlatList 
                style={{marginTop: 16}}
                keyExtractor={this._keyExtractor}
                data={this.state.content.task}
                extraData={this.state.content.task}
                renderItem={this._renderItem}>
              </FlatList>

              <View style={Styles.sectionSeparator}>
              </View>
          { this.state.content.image !== '' ?
          (<Image 
            style={{width: Metrics.WIDTH*0.9, height: Metrics.HEIGHT*0.3,marginTop:Metrics.HEIGHT*0.02, marginBottom:Metrics.HEIGHT*0.02}} 
              source={{uri: this.state.content.image}}
              resizeMode={'contain'} />) : (<View></View>)}              
              <Text style={Styles.sectionBlue}>Personal Note</Text>

              <TextInput
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                  height: 128,
                }}
                multiline={true}
                numberOfLines={10}
                placeholder={this._placeholderText}
                placeholderTextColor="#b7b7b7"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                textAlignVertical="top"
                keyboardType="default" 
                value={this.state.content.comment}
                onChangeText={(comment) => {
                  const contentCopy = Object.assign({}, this.state.content, { comment });

                  this.setState({ content: contentCopy });
                }} />
              <TouchableOpacity style={Styles.buttonRedeem} onPress={() => this.submitCompletion()}>
                <Text style={Styles.signInText}>SUBMIT</Text>
              </TouchableOpacity>
            </KeyboardAwareScrollView>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}
