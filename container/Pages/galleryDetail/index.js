
import React, { PropTypes, Component } from 'react';
import { 
  Platform, 
  StatusBar,
  Dimensions, 
  View, 
  Text, 
  TextInput,
  Image, 
  ImageBackground, 
  TouchableOpacity, 
  WebView,
  BackHandler, 
  I18nManager,
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
} from 'native-base';
import Swiper from 'react-native-swiper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';
import { Images } from '../../Themes';
// Screen Styles
import Styles from './styles';
import YouTube from 'react-native-youtube';

export default class GalleryDetail extends Component {
  _videoPlayer = null;
  _dummyText = "Lorem ipsum dolor sit amet, " 
    + "consectetur adipiscing elit. In porta ipsum laoreet leo aliquet semper. Vivamus purus dui, tincidunt et mauris vitae, " 
    + "semper aliquet elit. Duis tempus, urna ac bibendum mollis, augue ex auctor turpis, non volutpat enim augue vel nisl";

  componentWillMount() {
    var self = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      self.props.navigation.navigate('Dashboard')
      return true;
    });
  }

  onShouldStartLoadWithRequest = (navigator) => {
    if (navigator.url.indexOf('embed') !== -1) {
      return true;
    } else {
      if (this._videoPlayer) {
        this._videoPlayer.stopLoading(); //Some reference to your WebView to make it stop loading that URL
      }
      
      return false;
    }
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    const { navigation } = this.props;
    
    let navigationTitle = navigation.getParam('title', '');
    let date = navigation.getParam('date', '');
    let description = navigation.getParam('description', '');

    let mediaType = navigation.getParam('type', 'video');
    let isVideo = mediaType == 'video';

    let images = navigation.getParam('images', []);
    let youtubeUrl = navigation.getParam('youtubeUrl', '');
    
    let videoId = youtubeUrl.replace('https://www.youtube.com/watch?v=', '');
    console.log(videoId);

    var renderer;

    if (Platform.OS === 'ios') {
      renderer = 
      <Content>
        <View style={Styles.mediaContainer}>
          {
            isVideo ? 
            <YouTube
              videoId={videoId}   // The YouTube video ID
              play={true}             // control playback of video with true/false
              showFullscreenButton={true}       // control whether the video should play in fullscreen or inline
              loop={true}             // control whether the video should loop when ended
              apiKey="AIzaSyDuGyRLklDH8_JsiAto4X7Naw50C-pu628"
              onReady={e => this.setState({ isReady: true })}
              onChangeState={e => this.setState({ status: e.state })}
              onChangeQuality={e => this.setState({ quality: e.quality })}
              onError={e => this.setState({ error: e.error })}

              style={{ alignSelf: 'stretch', height: 200 }}
              />
            :
            <Swiper showsButtons={true}
              autoplay={true}
              autoplayTimeout={2.5}>
                {
                  images.map((item, index) => {
                    return (
                      <View key={index}>
                        <Image 
                          style={Styles.mediaContent} 
                          source={{uri: item}} 
                          resizeMode='contain'
                        />
                      </View>
                    )
                  })
                }
            </Swiper>
          }
        </View>
        <View style={Styles.descriptionContainer}>
          {/* <Text style={Styles.date}>{date}</Text> */}
          <Text style={Styles.title}>{navigationTitle}</Text>
          <HTML html={description} />
        </View>
      </Content>;
    } else {
      renderer = 
      <Content>
        {
          isVideo ? 
          <YouTube
            videoId={videoId}   // The YouTube video ID
            play={true}             // control playback of video with true/false
            showFullscreenButton={true}       // control whether the video should play in fullscreen or inline
            loop={true}             // control whether the video should loop when ended
            apiKey="AIzaSyDuGyRLklDH8_JsiAto4X7Naw50C-pu628"
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onError={e => this.setState({ error: e.error })}

            style={{ alignSelf: 'stretch', height: 200 }}
          />
          :
          <Swiper showsButtons={true}
            autoplay={true}
            style={Styles.mediaContainer}
            autoplayTimeout={2.5}>
              {
                images.map((item, index) => {
                  return (
                    <View key={index}>
                      <Image 
                        style={Styles.mediaContent} 
                        source={{uri: item}} 
                        resizeMode='contain'
                      />
                    </View>
                  )
                })
              }
          </Swiper>
        }
        
        <View style={Styles.descriptionContainer}>
          <Text style={Styles.date}>{date}</Text>
          <Text style={Styles.title}>{navigationTitle}</Text>
          <HTML html={description} />
        </View>
      </Content>
    }
    return (
      <Container>
        <ImageBackground style={Styles.imgContainer} source={imageUri}>
          <Header style={Styles.header}>
            <Left style={Styles.left}>
              <TouchableOpacity style={Styles.backArrow} onPress={()=>this.props.navigation.navigate('Dashboard')}>
                <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
              </TouchableOpacity>
            </Left>
            <Body style={Styles.body}>
              <Text style={Styles.titleNavigationBar}>{navigationTitle}</Text>
            </Body>
            <Right style={Styles.right}/>
          </Header>
          {renderer}
        </ImageBackground>
      </Container>
    );
  }
}
