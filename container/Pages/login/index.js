
import React, { PropTypes, Component } from 'react';
import { 
  View, 
  Platform, 
  Dimensions, 
  Animated, 
  Keyboard, 
  Image, 
  Text, 
  TextInput, 
  TouchableOpacity, 
  ImageBackground, 
  Alert,
  BackHandler, 
  I18nManager, 
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
  Button, 
  Title,
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Images } from '../../Themes';
import styles from './styles';
import Loader from '../../libs/presentation/loader';
import { AuthService } from '../../libs/api';
import { TokenStorage, ProfileStorage } from '../../libs/storage';

export default class Login extends Component {
  keyboardHeight = new Animated.Value(0);
  keyboardWillShowSub = null;
  keyboardWillHideSub = null;
  duration = null;

  constructor(props) {
    super(props);

    this._populateDefaultState();
  }

  _populateDefaultState = async () => {
    this.state = {
      hidePassword: true,
      loading: false,
      user: {
        email: '',
        password: '',
      },
    };
  }

  componentWillMount() {
    let that = this;

    if (Platform.OS == 'android') {
      that.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', that.keyboardWillShow);
      that.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', that.keyboardWillHide);
    } else {
      that.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', that.keyboardWillShow);
      that.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', that.keyboardWillHide);
    }

    BackHandler.addEventListener('hardwareBackPress', function() {
      that.props.navigation.navigate('Home')

      return true;
    });
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    console.log(event);
    console.log(this.keyboardHeight);

    let that = this;

    that.duration = event.duration;

    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height,
      }),
    ]).start();
  };

  keyboardWillHide = (event) => {
    console.log(event);
    console.log(this.keyboardHeight);

    let that = this;

    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: that.duration,
        toValue: 0,
      }),
    ]).start();
  };

  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  login = () => {
    const { email, password } = this.state.user;
    let that = this;

    let clientValidation = true;

    if (!email) {
      clientValidation = false;
      message = "Email required";
    }

    if (!password) {
      clientValidation = false;
      message = "Password required";
    } else {
      if (password.length < 6) {
        clientValidation = false;
        message = "Min character required is 6";
      }
    }

    if (!clientValidation) {
      Alert.alert("Error", 
          "Validation: " + message,
          [
            {text: 'OK', onPress: () => {} },
          ],
          { cancelable: false }
        );

      return;
    }

    that._showProgress();

    AuthService
      .login({ email, password })
      .then((response) => {
        that._hideProgress();

        TokenStorage.setToken(response.token);
        ProfileStorage.setLocalProfile(response.user);

        if (response.user) {
          this.props.navigation.navigate('Dashboard');
        }
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        Alert.alert("Error", 
          "Wrong Email / Password",
          [
            {text: 'OK', onPress: () => that._hideProgress() },
          ],
          { cancelable: false }
        );
      });
  }

  _showProgress = () => {
    let newState = Object.assign({}, this.state, {loading: true});

    this.setState(newState);
  }

  _hideProgress = () => {
    let newState = Object.assign({}, this.state, {loading: false});

    this.setState(newState);
  }

  navigateBack = () => {
    this.props.navigation.navigate('Home')
  }

  navigateToRegister = () => {
    this.props.navigation.navigate('Register');
  }

  render() {
      const imageUri = Images.login_background;
      return (
        <Container>
          <ImageBackground style={styles.imgContainer} source={imageUri}>
            <Header style={styles.header}>
                <Left style={styles.left}>
                  <TouchableOpacity style={styles.backArrow} onPress={() => this.navigateBack()}>
                      <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
                  </TouchableOpacity>
                </Left>

                <Body style={styles.body}>
                  <Text style={styles.textTitle}>Sign In</Text>
                </Body>
                
                <Right style={styles.right}/>
            </Header>
            <Content>
              <Animated.View style={[styles.container, { paddingBottom: this.keyboardHeight }]}>
                <Loader loading={this.state.loading} />

                <View style={styles.logoSec}>
                    <Text style={styles.welcome}>Welcome!</Text>
                    <Text style={styles.description}>Please fill in the fields to continue using this apps</Text>    
                </View>

                <TextInput style={styles.textInput}
                  secureTextEntry = {false}
                  placeholder = "Email Address"
                  placeholderTextColor = "#b7b7b7"
                  underlineColorAndroid = "transparent"
                  autoCapitalize = "none"
                  textAlign={I18nManager.isRTL ? 'right' : 'left'}
                  keyboardType = "default"
                  onChangeText={(email) => {
                    const param = Object.assign({}, this.state.user, { email });
                    this.setState({ user: param });
                  }}
                />

                <TextInput style={styles.textInput} 
                  secureTextEntry = {true}
                  placeholder = "Password"
                  placeholderTextColor = "#b7b7b7"
                  underlineColorAndroid = "transparent"
                  autoCapitalize = "none"
                  textAlign={I18nManager.isRTL ? 'right' : 'left'}
                  keyboardType = "default"
                  onChangeText={(password) => {
                    const param = Object.assign({}, this.state.user, { password });
                    this.setState({ user: param });
                  }}
                />

                <TouchableOpacity style={styles.buttonSignIn} onPress={() => this.login()}>
                  <Text style={styles.signInText}>Submit</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.navigateToRegister()}>
                  <Text style={styles.registerText}>No account yet? Register Here</Text>
                </TouchableOpacity>
              </Animated.View>  
            </Content>
          </ImageBackground>
        </Container>
      );
  }
}
