import React, { Component } from "react";
import { 
  View, 
  Image,
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import Styles from '../dashboard/HomeStyle';

class GalleryListItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.item.id);
  }

  render() {
    const item = this.props.item;
    
    return (
      <Card transparent>
        <CardItem cardBody button onPress={this._onPress}>
          <Body>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <View style={{flex: 1, flexDirection: 'column', marginTop: 16, marginBottom: 16, marginLeft: 8, marginRight: 8}}>
                {/* <Text>
                  {item.date}
                </Text> */}
                <Text style={{fontWeight: 'bold'}}>
                  {item.title}
                </Text>
                <Text>
                  {item.shortDescription}
                </Text>
              </View>
              <Image style={{width: 96, height: 96, marginRight: 8}} 
                source={{uri: item.thumbnail}}
                resizeMode={'contain'}>
              </Image>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

export default GalleryListItem
