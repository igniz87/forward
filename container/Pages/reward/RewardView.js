import React, { Component } from "react";
import { 
  View, 
  ListView,
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from '../dashboard/GiftStyle';
import RewardListItem from "../reward/RewardListItem"

class RewardView extends React.PureComponent {
  _ds = null;

  constructor(props) {
    super(props);

    this._ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: this._ds.cloneWithRows(this.props.item.data),
    };
  }

  _renderRow = (item) => {
    return (
      <RewardListItem
        item={item}
        disabled={this.props.disabled}
        onPressItem={this._onRowSelected}
      />
    )
  }

  _onRowSelected = (id) => {
    console.log("Pressed row with id: " + id);
    let reward = this.props.item.data.find(function(element) {
      return element.id == id;
    });
    this.props.navigation.navigate('RewardDetail', {
      reward: reward, 
      refresh: this.props.refresh
    });
  };

  render() {
    const item = this.props.item;
    let newContent = this._ds.cloneWithRows(this.props.item.data);

    return (
      <Card transparent>
        <CardItem header>
          <Text style={Styles.sectionBlue}>{item.title}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <ListView
                dataSource={newContent}
                extraData={this.state.dataSource}
                renderRow={this._renderRow} />
            </View>
          </Body>
        </CardItem>
        <CardItem footer>
          <View style={Styles.cardFooterContainer}>
            <View style={Styles.sectionSeparator}>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default RewardView
