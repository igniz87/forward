import React, { Component } from "react";
import { 
  View, 
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import { Colors } from '../../Themes';
import Styles from '../dashboard/GiftStyle';

class RewardListItem extends React.PureComponent {
  _onPress = () => {
    if (this.props.onPressItem && this.props.disabled !== 'true') {
      this.props.onPressItem(this.props.item.id);
    }
  }

  render() {
    const item = this.props.item;
    const textColor = this.props.disabled === 'true' ? Colors.gray : item.color;

    return (
      <Card>
        <CardItem cardBody button onPress={this._onPress}>
          <Body>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 0, marginBottom: 0, marginLeft: 0, marginRight: 8}}>
              <View style={{width: 8, backgroundColor: item.color}}>
              </View>
              <View style={{marginTop: 16, marginBottom: 16, marginLeft: 8, marginRight: 8}}>
              { this.props.disabled === 'true' ?
                (<Text style={{color: textColor}}>
                  {item.date}
                </Text>  ) : (<View></View>) }   
                <Text style={{fontWeight: 'bold', color: textColor}}>
                  {item.title}
                </Text>
              </View>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

export default RewardListItem
