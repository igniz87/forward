
import React, { PropTypes, Component } from 'react';
import { 
  Platform, 
  StatusBar, 
  Dimensions, 
  View, 
  Text, 
  TextInput,
  Image, 
  ImageBackground, 
  TouchableOpacity, 
  WebView,
  BackHandler, 
  I18nManager,
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
} from 'native-base';
import Swiper from 'react-native-swiper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';
import { Images,Metrics } from '../../Themes';
// Screen Styles
import Styles from './styles';
import { EventService } from '../../libs/api';
import API from '../../API';
export default class EventDetail extends Component {
  _dummyText = "Lorem ipsum dolor sit amet, " 
    + "consectetur adipiscing elit. In porta ipsum laoreet leo aliquet semper. Vivamus purus dui, tincidunt et mauris vitae, " 
    + "semper aliquet elit. Duis tempus, urna ac bibendum mollis, augue ex auctor turpis, non volutpat enim augue vel nisl";

  constructor(props) {
    super(props);

    const { navigation } = this.props;
    let id = navigation.getParam('id', -1);

    this._populateDefaultState();
    this._loadContent(id);
  }

  _populateDefaultState = async () => {
    this.state = {
      loading: false,
      refreshing: false,
      content: {
        title: '',
        description: null,
        date: ''
      }
    };
  }

  _loadContent = (id) => {
    EventService
      .getEventDetail(id)
      .then(async (response) => {
        let newState = Object.assign({}, this.state);

        console.log(response.data);

        newState.content.title = response.data.title;
        newState.content.description = response.data.description;
        if(response.data.image !== null){
          newState.content.image = `${API.baseURL}/static${response.data.image}`;
        }
        else{
          newState.content.image = '';
        }
        
        newState.content.place = response.data.place;
        newState.content.time = response.data.time;
        newState.content.date = response.data.eventDatePreformated;
        
        // alert(`${API.baseURL}/static${response.data.image}`);
        await this.setState(newState);
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching event detail failed: " + message);
        console.log(error.response);
      });
  }

  componentWillMount() {
    var that = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      that.navigateBack();

      return true;
    });
  }

  navigateBack = () => {
    this.props.navigation.navigate('Dashboard')
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <ImageBackground style={Styles.imgContainer} source={imageUri}>
          <Header style={Styles.header}>
            <Left style={Styles.left}>
              <TouchableOpacity style={Styles.backArrow} onPress={() => this.navigateBack()}>
                <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
              </TouchableOpacity>
            </Left>
            <Body style={Styles.body}>
              <Text style={Styles.titleNavigationBar}>{this.state.content.title}</Text>
            </Body>
            <Right style={Styles.right}/>
          </Header>
          <Content>
          { this.state.content.image !== '' ?
          (<Image 
              style={{width: Metrics.WIDTH*0.9, height: Metrics.HEIGHT*0.3,marginLeft:Metrics.WIDTH*0.05,marginTop:Metrics.HEIGHT*0.02, marginBottom:Metrics.HEIGHT*0.02}} 
                source={{uri: this.state.content.image}}
                resizeMode={'contain'} />) : (<View></View>)}
              <View style={Styles.descriptionContainer} >
                <Text style={Styles.title}>{this.state.content.title}</Text>
                <Text style={Styles.date}>🗓 {this.state.content.date} ⏰ {this.state.content.time}</Text>
                <Text style={Styles.place}>📍 {this.state.content.place}</Text>
                { this.state.content.description !== null ?
                (<HTML html={this.state.content.description} />  ) : (<View></View>) }                
              </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}
