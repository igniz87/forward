import { Platform, StyleSheet } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  imgContainer: {
    width: Metrics.WIDTH,
  },
  sectionHeader: {
    color: Colors.sectionHeader,
    fontWeight: 'bold',
    fontSize: 24,
  },
  description: {
    fontSize: Fonts.moderateScale(14),
    fontWeight:'500',
    marginTop: (Metrics.HEIGHT* 0.01),
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    lineHeight: 24,
  },
});

export default styles;
