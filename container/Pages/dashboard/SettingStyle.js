
import { Platform, StyleSheet, Dimensions } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  imgContainer: {
    width: Metrics.WIDTH,
  },
  description: {
    width: (Metrics.WIDTH * 0.7),
    color: Colors.black,
    textAlign: 'center',
    fontSize: Fonts.moderateScale(16),
    fontWeight:'500',
    marginTop: (Metrics.HEIGHT* 0.05),
    marginBottom: (Metrics.HEIGHT* 0.02),
    alignSelf: 'center',
	  fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
  buttonSignIn: {
    backgroundColor: "#70DFE8",
    borderRadius: 20,
    marginTop: 28,
    padding: 12,
    alignSelf: 'center',
    width: (Metrics.WIDTH * 0.4),
  },
  signInText: {
    color: Colors.white,
    textAlign: "center",
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
});
export default styles;
