import React, { Component } from "react";
import {
  Platform,
  Dimensions,
  StatusBar,
  View,
  ScrollView,
  Image,
  ImageBackground,
} from "react-native";
import { Container, Card, CardItem, Text, Body } from "native-base";
import { LinearGradient } from "react-native-linear-gradient";
import HTML from 'react-native-render-html';
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import MonthView from "../home/MonthView";
import Styles from "./ForwardStyle";
import { CalendarService, QuoteService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';

export default class Forward extends Component {
  _totalChallengeDay = 30;
  _quotes = [];
  
  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = () => {
    this.state = {
      index: "",
      loading: false,
      refreshing: false,
      content: {
        totalDayCompleted: 0
      }
    };
  }

  _loadContent = async () => {
    let that = this;

    QuoteService
      .getQuotes()
      .then(async (response) => {
        that._quotes = response.datas;

        that._loadCalendarDetail();
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching quotes failed: " + message);
        console.log(error);
      });
  }

  _loadCalendarDetail = async () => {
    let that = this;

    CalendarService
      .getCalendar()
      .then(async (response) => {
        let quote = null;

        console.log(that._quotes);

        if (response.datas.length == that._totalChallengeDay) {
          quote = that._quotes.find(function(element) {
            return element.day == that._totalChallengeDay;
          });
        } else {
          quote = that._quotes.find(function(element) {
            return element.day > response.datas.length;
          });
        }

        this.setState({
          index: "",
          loading: false,
          refreshing: false,
          content: {
            quote: quote.description,
            totalDayCompleted: response.datas.length
          }
        });
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching calendar failed: " + message);
        console.log(error);
      });
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <ScrollView>
        <Container style={{backgroundColor: '#FFFFFF'}}>
          <Card transparent>
            <CardItem header>
              <Text style={Styles.sectionHeader}>30 Days Challenges</Text>
            </CardItem>
            <CardItem>
              <Body>
                <HTML html={this.state.content.quote} />
              </Body>
            </CardItem>
            <CardItem style={{backgroundColor: Colors.transparent}}>
              <Body style={{
                  flex: 1,
                  flexDirection: 'row',
                }}>
                <MonthView 
                  navigation={this.props.navigation} 
                  totalDayCompleted={this.state.content.totalDayCompleted} 
                  readOnly={false} 
                  refresh={this._loadContent} />
              </Body>
            </CardItem>
          </Card>
        </Container>
      </ScrollView>
    );
  }
}
