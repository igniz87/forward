import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StatusBar,
  Platform,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  ListView
} from "react-native";
import {
  Container,
  Button,
  Icon,
  Right,
  Header,
  Left,
  Body,
  Title
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import styles from "./DefaultStyle";

export default class Default extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: ""
    };

  }

  render() {
    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }
    
    return (
      <Container style={{ backgroundColor: "white" }}>
        <View style={{ backgroundColor: "rgba(0,0,0,0.5)" }}>
          <View style={styles.slidesec}>
            
            <Text>Default</Text>
                
          </View>
        </View>
      </Container>
    );
  }
}
