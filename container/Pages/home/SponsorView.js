import React, { Component } from "react";
import { 
  View,
  Image,
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from '../dashboard/HomeStyle';

class SponsorView extends React.PureComponent {
  render() {
    const item = this.props.item;
    const sponsors = item.data;

    let sponsor1Url = sponsors[0] ? sponsors[0].image : '';
    let sponsor2Url = sponsors[1] ? sponsors[1].image : '';
    let sponsor3Url = sponsors[2] ? sponsors[2].image : '';
    let sponsor4Url = sponsors[3] ? sponsors[3].image : '';
    let sponsor5Url = sponsors[4] ? sponsors[4].image : '';
    let sponsor6Url = sponsors[5] ? sponsors[5].image : '';

    return (
      <Card transparent>
        <CardItem header>
          <Text style={Styles.sectionBlue}>{item.title}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <View style={{flex: 1, flexDirection: 'row',marginLeft:-(Metrics.WIDTH*0.05),marginTop:-(Metrics.HEIGHT*0.03)}}>
            <Image resizeMode={'contain'} style={Styles.sponsorsImages} source={Images.sponsors}/>
              {/* <Image
                  style={{alignSelf: 'center', height: 120, width: 160}}
                  source={{uri: sponsor1Url}}
                  resizeMode={'contain'} />
              
              <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    source={{uri: sponsor2Url}}
                    style={{alignSelf: 'center', height: 60, width: 60}}
                    resizeMode={'contain'} />
                  <Image
                    source={{uri: sponsor3Url}}
                    style={{alignSelf: 'center', height: 60, width: 60}}
                    resizeMode={'contain'} />
                </View>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start'}}>
                  <Image
                    source={{uri: sponsor4Url}}
                    style={{alignSelf: 'center', height: 60, width: 40}}
                    resizeMode={'contain'} />
                  <Image
                    source={{uri: sponsor5Url}}
                    style={{alignSelf: 'center', height: 60, width: 40}}
                    resizeMode={'contain'}/>
                  <Image
                    source={{uri: sponsor6Url}}
                    style={{alignSelf: 'center', height: 60, width: 40}}
                    resizeMode={'contain'} />
                </View>
              </View> */}
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

export default SponsorView