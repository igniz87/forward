import React, { Component } from "react";
import { 
  View, 
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import Styles from '../dashboard/HomeStyle';

class EventListItem extends React.PureComponent {
  _onPress = () => {
    if (this.props.onPressItem) {
      this.props.onPressItem(this.props.item.id);
    }
  }

  render() {
    const item = this.props.item;

    return (
      <Card>
        <CardItem cardBody button onPress={this._onPress}>
          <Body>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 0, marginBottom: 0, marginLeft: 0, marginRight: 8}}>
              <View style={{width: 8, backgroundColor: item.color}}>
              </View>
              <View style={{marginTop: 16, marginBottom: 16, marginLeft: 8, marginRight: 8}}>
                <Text>
                🗓 {item.date}
                </Text>
                <Text style={{fontWeight: 'bold'}}>
                  {item.title}
                </Text>
                <Text>
                📍 {item.place}
                </Text>
                <Text>
                ⏰ {item.time}
                </Text>
              </View>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

export default EventListItem
