import React, { Component } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  FlatList,
  ImageBackground,
  Text,
  TouchableOpacity,
} from "react-native";
import { Container } from "native-base";
import { LinearGradient } from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import { DayChallengeProvider } from "../../libs/provider"

const numColumns = 6;
const monthMargin = 16;

const size = (Dimensions.get('window').width - (monthMargin * 2)) / numColumns;
// only applied to this view
const styles = StyleSheet.create({
  itemContainer: {
    width: size,
    height: size,
    borderWidth: 1,
    borderColor: 'transparent',
  },
  itemContainerBlur: {
    backgroundColor: 'transparent',
  },
  itemContainerBlock: {
    backgroundColor: Colors.forwardLightBlue,
  },
  item: {
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  itemBlur: {
    color:'white',
    textAlign:'center',
    justifyContent:'center',
    marginTop:18,
    fontSize:14
  },
  itemBlock: {
    // color: Colors.black
    color:'white',
    textAlign:'center',
    justifyContent:'center',
    marginTop:18,
    fontSize:14
  },
});

class MonthView extends React.PureComponent {
  _list = [];
  _readOnly = false;

  constructor(props) {
    super(props);

    this._list = DayChallengeProvider.getDays(0);
    this._readOnly = this.props.readOnly;

    this.state = { dimensions: undefined, };
  }

  _keyExtractor = (item, index) => index

  _renderItem = ({ item }) => {    
    let block = true;
    
    if(item.day===1) {
      block = false;
    }
    else {
      if(item.day <= this.props.totalDayCompleted+1) {
        block = false;
      }
      else {
        block = true;
      }
    }
    return (
      <TouchableOpacity
        disabled={block}
        style={
          item.unlocked ? 
          StyleSheet.flatten([styles.itemContainer, styles.itemContainerBlur]) : 
          StyleSheet.flatten([styles.itemContainer, styles.itemContainerBlock])
        }
        onPress={() => {
          this._onDaySelected(item.id);
        }}>
        <Text style={
          item.unlocked ? 
          StyleSheet.flatten([styles.item, styles.itemBlur]) : 
          StyleSheet.flatten([styles.item, styles.itemBlock])
        }>
          {item.id}
        </Text>
      </TouchableOpacity>
    )
    
  }

  _onDaySelected = (dayId) => {
    if (!this._readOnly) {
      console.log(dayId);

      this.props.navigation.navigate('TaskDetail', {
        day: dayId, 
        refresh: this.props.refresh
      });
    } else {
      this.props.navigateToForward();
    }
  };

  render() {
    let totalDayCompleted = this.props.totalDayCompleted;

    if (totalDayCompleted) {
      this._list = DayChallengeProvider.getDays(totalDayCompleted);
    } else {
      this._list = DayChallengeProvider.getDays(0);
    }

    let imageUri = Images.sheep;
    
    return (
      <ImageBackground style={{
          flex: 1, 
          flexDirection: 'column',
          width: '100%', height: '100%'
        }} source={imageUri}>
        <FlatList
          keyExtractor={this._keyExtractor}
          data={this._list}
          extraData={this._list}
          renderItem={this._renderItem}
          scrollEnabled={true}
          numColumns={numColumns}
        />
      </ImageBackground>
    );
  }
}

export default MonthView
