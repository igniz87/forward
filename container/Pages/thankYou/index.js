import React, { PropTypes, Component } from 'react';
import { Text, 
  View, 
  Platform, 
  Dimensions, 
  Image, 
  ImageBackground, 
  TextInput, 
  TouchableOpacity, 
  FlatList, 
  ListItem, 
  BackHandler, 
  I18nManager, 
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
  CheckBox
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Images, Colors } from '../../Themes';
// Screen Styles
import Styles from './styles';
import { MessageService } from '../../libs/api';

export default class ThankYou extends Component {
  _challengeCompletedContent = "Gift Rewarded: Lorem ipsum dolor sit amet, " 
    + "consectetur adipiscing elit. In porta ipsum laoreet leo aliquet semper. Vivamus purus dui, tincidunt et mauris vitae, " 
    + "semper aliquet elit. Duis tempus, urna ac bibendum mollis, augue ex auctor turpis, non volutpat enim augue vel nisl";

  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = async () => {
    this.state = {
      loading: false,
      refreshing: false,
      content: {
        title: '',
        description: '',
      }
    };
  }

  _loadContent = async () => {
    MessageService
      .getAllTaskCompletedMessage()
      .then(async (response) => {
        let newState = Object.assign({}, this.state);
        
        newState.content.title = '';
        newState.content.description = response.wording;

        this.setState(newState);
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching message failed: " + message);
        console.log(error.response);
      });
  }

  componentWillMount() {
    var self = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      self.props.navigation.navigate('Dashboard');

      return true;
    });
  }

  _keyExtractor = (item, index) => index

  navigateBack = () => {
    this.props.navigation.navigate('Dashboard');
  }

  render() {
    const imageUri = Images.background_main;
    let navigationTitle = this.props.navigation.getParam('title', 'Thank you');
    let hasGift = this.props.navigation.getParam('hasGift', false);

    return (
      <Container>
          <ImageBackground style={Styles.imgContainer} source={imageUri}>
            <Header style={Styles.header}>
                <Left style={Styles.left}>
                  <TouchableOpacity style={Styles.backArrow} onPress={() => this.navigateBack()}>
                    <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
                  </TouchableOpacity>
                </Left>

                <Body style={Styles.body}>
                  <Text style={Styles.textTitle}>Thank you</Text>
                </Body>
                
                <Right style={Styles.right}/>
            </Header>
            <Content>
                <View style={Styles.actionDescription}>
                  <Text style={Styles.title}>Thank you for completing all the challenge!</Text>
                  <Text style={Styles.description}>{this.state.content.description}</Text>
                </View>
                
                <TouchableOpacity style={Styles.buttonRedeem} onPress={() => this.navigateBack()}>
                  <Text style={Styles.signInText}>BACK</Text>
                </TouchableOpacity>
            </Content>
          </ImageBackground>
      </Container>
    );
  }
}
