import axios from 'axios';
import API from '../../API';

const login = function (data) {
  return axios.post(API.login, data)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      // alert(error.response.data.msg);
      return Promise.reject(error);
    });
};

const register = function (data) {
  return axios.post(API.register, data)
    .then(response => response.data)
    .catch((error) => {
      // alert(error.response.data.msg);
      return Promise.reject(error);
    });
};

export {
  login,
  register,
};