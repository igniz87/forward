import axios from 'axios';
import API from '../../API';

const getEventDetail = function (id) {
  return axios.get(API.event + '/' + id)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  getEventDetail,
};