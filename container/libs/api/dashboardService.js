import axios from 'axios';
import API from '../../API';

const dashboard = function () {
  return axios.get(API.dashboard)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  dashboard,
};