const getDays = function(totalDayCompleted) {
  let result = [];
  let startDay = 1;

  for (var i = 1; i <= 30; i++) {
    let day = {
      id: i,
      unlocked: i <= totalDayCompleted,
      day: i
    }

    result.push(day);
  }

  return result;
}

export {
  getDays,
}
